# Description

College students often find it difficult to decide which classes to take or what
they want to do once they graduate. Gradly is an app which aims to help guide
students and graduates in their decision making process by providing insights
into the skills and coursework required for particular jobs. Users can find
recommendations on classes to take, employers to apply to, and skills to learn
by using Gradly's proprietary matching algorithms which analyze the education
and employment records of dozens of users (1.67 dozen, to be precise
:stuck_out_tongue:). Our goal is to reduce the stress of college life by making
it easy to pick relevant classes and apply for jobs which fit your skillset.

# Usefulness

This application is useful for new graduates and current students who want to
explore useful career statistics such as salaries, past career employment
history, relevant skillset in order to enter a particular industry or connect
with research partners or peers with similar interests. It is also meant to help
users learn more about a particular industry. Eg. What is the career outlook?
How to reach your career goals? What kinds of experience and skills are helpful?
The application will serve primarily as a data analysis and profile sharing
platform, with possibilities for extension by adding public posts (a la
LinkedIn) and direct messaging.

The primary difference between this application and competitors is the targeted
nature. We are interested only in a user's education background, career
trajectory, some personal interests/hobbies, and professional skills.

# Database Overview

## Data Description

Our database contains various data about users' education and work history. The
primary entity of the database is a User, which is used to connect the different
pieces of information. We chose not to store any personally identifiable
information (like name or student ID), and instead assign each user a unique ID.
Along with this UserID, we also store a list of skills reported by the user,
similar to how LinkedIn records skills for displaying on your profile.

Each user may also report zero or more education or work history items. For each
education history item, we collect the university name, degree type, graduation
year, major, GPA, and a list of courses. Each course is given a name and number,
intended to match the school's course catalog, and associated with the
appropriate university.  When a new user course is added to a user's profile, an
internal "enrollment" entity is created to record the relationship.

For each experience in a user's work history, we collect the employer's name,
user's job title, user's industry (area of work, which may not match the
company's industry), salary, and a rating of the user's experience.

## ER Diagram and Schema

![ER_design.png](ER_design.png)

```sql
CREATE TABLE user (
   id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   skills TEXT
);
CREATE TABLE university (
   name VARCHAR(255) NOT NULL COLLATE NOCASE,
   PRIMARY KEY (name)
);
CREATE TABLE position (
   id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   "employerName" VARCHAR(255) COLLATE NOCASE,
   "jobTitle" VARCHAR(50) NOT NULL COLLATE NOCASE,
   FOREIGN KEY("employerName") REFERENCES employer (name) ON UPDATE CASCADE
);
CREATE TABLE education (
   id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   "userID" INTEGER NOT NULL,
   university VARCHAR(255) NOT NULL COLLATE NOCASE,
   year INTEGER NOT NULL,
   degree VARCHAR(10),
   major VARCHAR(255) COLLATE NOCASE,
   gpa NUMERIC(3, 2),
   FOREIGN KEY(university) REFERENCES university (name) ON UPDATE CASCADE,
   FOREIGN KEY("userID") REFERENCES user (id) ON DELETE CASCADE,
   CONSTRAINT degreetype CHECK (degree IN ('associate', 'bachelor', 'master', 'phd'))
);
CREATE TABLE experience (
   "userID" INTEGER NOT NULL,
   "positionID" INTEGER NOT NULL,
   salary INTEGER,
   type VARCHAR(9),
   rating INTEGER,
   industry VARCHAR(36),
   PRIMARY KEY ("userID", "positionID"),
   FOREIGN KEY("positionID") REFERENCES position (id),
   FOREIGN KEY("userID") REFERENCES user (id) ON DELETE CASCADE,
   CONSTRAINT rating CHECK (rating BETWEEN 0 AND 10),
   CONSTRAINT jobtype CHECK (type IN ('intern', 'research', 'co-op', 'part-time', 'full-time')),
   CONSTRAINT industry CHECK (industry IN ('Accounting', 'Airlines/Aviation', 'Alternative Dispute Resolution', 'Alternative Medicine', 'Animation', 'Apparel & Fashion', 'Architecture & Planning', 'Arts and Crafts', 'Automotive', 'Aviation & Aerospace', 'Banking', 'Biotechnology', 'Broadcast Media', 'Building Materials', 'Business Supplies and Equipment', 'Capital Markets', 'Chemicals', 'Civic & Social Organization', 'Civil Engineering', 'Commercial Real Estate', 'Computer & Network Security', 'Computer Games', 'Computer Hardware', 'Computer Networking', 'Computer Software', 'Construction', 'Consumer Electronics', 'Consumer Goods', 'Consumer Services', 'Cosmetics', 'Dairy', 'Defense & Space', 'Design', 'Education Management', 'E-Learning', 'Electrical/Electronic Manufacturing', 'Entertainment', 'Environmental Services', 'Events Services', 'Executive Office', 'Facilities Services', 'Farming', 'Financial Services', 'Fine Art', 'Fishery', 'Food & Beverages', 'Food Production', 'Fund-Raising', 'Furniture', 'Gambling & Casinos', 'Glass, Ceramics & Concrete', 'Government Administration', 'Government Relations', 'Graphic Design', 'Health, Wellness and Fitness', 'Higher Education', 'Hospital & Health Care', 'Hospitality', 'Human Resources', 'Import and Export', 'Individual & Family Services', 'Industrial Automation', 'Information Services', 'Information Technology and Services', 'Insurance', 'International Affairs', 'International Trade and Development', 'Internet', 'Investment Banking', 'Investment Management', 'Judiciary', 'Law Enforcement', 'Law Practice', 'Legal Services', 'Legislative Office', 'Leisure, Travel & Tourism', 'Libraries', 'Logistics and Supply Chain', 'Luxury Goods & Jewelry', 'Machinery', 'Management Consulting', 'Maritime', 'Market Research', 'Marketing and Advertising', 'Mechanical or Industrial Engineering', 'Media Production', 'Medical Devices', 'Medical Practice', 'Mental Health Care', 'Military', 'Mining & Metals', 'Motion Pictures and Film', 'Museums and Institutions', 'Music', 'Nanotechnology', 'Newspapers', 'Non-Profit Organization Management', 'Oil & Energy', 'Online Media', 'Outsourcing/Offshoring', 'Package/Freight Delivery', 'Packaging and Containers', 'Paper & Forest Products', 'Performing Arts', 'Pharmaceuticals', 'Philanthropy', 'Photography', 'Plastics', 'Political Organization', 'Primary/Secondary Education', 'Printing', 'Professional Training & Coaching', 'Program Development', 'Public Policy', 'Public Relations and Communications', 'Public Safety', 'Publishing', 'Railroad Manufacture', 'Ranching', 'Real Estate', 'Recreational Facilities and Services', 'Religious Institutions', 'Renewables & Environment', 'Research', 'Restaurants', 'Retail', 'Security and Investigations', 'Semiconductors', 'Shipbuilding', 'Sporting Goods', 'Sports', 'Staffing and Recruiting', 'Supermarkets', 'Telecommunications', 'Textiles', 'Think Tanks', 'Tobacco', 'Translation and Localization', 'Transportation/Trucking/Railroad', 'Utilities', 'Venture Capital & Private Equity', 'Veterinary', 'Warehousing', 'Wholesale', 'Wine and Spirits', 'Wireless', 'Writing and Editing'))
);
CREATE TABLE enrollment (
   "educationID" INTEGER NOT NULL,
   "courseID" INTEGER NOT NULL,
   PRIMARY KEY ("educationID", "courseID"),
   FOREIGN KEY("courseID") REFERENCES course (id),
   FOREIGN KEY("educationID") REFERENCES education (id) ON DELETE CASCADE
);
CREATE TABLE employer (
   name VARCHAR(255) NOT NULL,
   PRIMARY KEY (name)
);
CREATE TABLE course (
   id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   "universityName" VARCHAR(255) COLLATE NOCASE,
   "courseTitle" VARCHAR(255) NOT NULL COLLATE NOCASE,
   "courseNumber" VARCHAR(10) NOT NULL COLLATE NOCASE,
   FOREIGN KEY("universityName") REFERENCES university (name) ON UPDATE CASCADE
);
```

## Data Collection

To seed the database with our initial dataset, we surveyed friends and
classmates. We asked each of them for a list of classes they have taken and
confirmed the details of their degree (graduation year, major, GPA if they are
comfortable sharing). We also asked for a summary of their work and research
experience since attending their university. We limit work experience to
positions held in college to improve relevance in the recommendation engine.

The skills on each user's profile are self-reported and often taken directly
from the user's LinkedIn profile.

## NoSQL

We did not implement NoSQL in our project.

## Data Storage

We store all of our data in a relational database using SQLite in Python. We
chose SQLite because it has native integration with Python, and our data fit
well in a relational model. It was also simpler to manage a single DB file
rather than setting up a separate MySQL server.

At first, we used SQLalchemy as a DB interface, before realizing we had to
implement everything by writing SQL statements directly. SQLalchemy was nice
because it offers a high level abstraction of the database entity relationships.
For instance, we could have a Model object for Course and University where
Course has an attribute referencing the University at which it is offered.
SQLalchemy provides a mechanism to back-reference University to each course,
allowing easy retrieval of all Courses offered at a particular University.

We then migrated to using Python's built in SQLite API, which basically just
offers methods to execute SQL commands. To achieve the same functionality as the
example above, we would have to declare a helper method to run a SELECT query
over the course table. For some situations, a VIEW may be applicable, but many
situations required some form of parameterization. Since SQLite does not support
functions or stored procedures, we were not able to define functions in the DB
engine to accomplish the same task.

When converting our ER design into a relational schema, we added several
additional tables to the set of original entities.  We use a table to record a
user's experience, indexed by the userID and positionID. We also use a table to
record graduation data, and assign each one a unique index. This index is
necessary because an enrollment is not really a relationship between a User and
a Course, but a particular graduation of the User. Initially, we simply recorded
the userID and courseID in each enrollment record, but this was not sufficient
for users with multiple items in their education history – we were unable to
distinguish between classes taken at each institution (particularly if the user
attended the same university twice).  By assigning each enrollment to a
graduation, and each graduation to a user, we are able to separate the courses
taken at each institution easily.

# Features

## Overview

### Basic Features

1. User profiles
    - Users may enter their information on the home page to record their
      educational history, including courses taken at each institution, as well
      as their work experience. Users may also include a list of skills which
      they feel defines them.

2. Mutability
    - Users may edit their profiles at any time by adding, changing, or removing
      information. The system is robust, even to changes in the identifying
      information such as course number or graduation year. If a user wishes to
      remove all of their information, we also provide a deletion capability.
    - <span style="color:red"><b>Note: we have not implemented any form of
      authentication or access control. Thus, it is possible for one user to
      delete any other user from the database. This is not intended to be a
      production implementation and some form of account authentication would be
      necessary in a real deployment.</b></span>

3. Searchable
    - Users may search for other users by entering the userID generated at
      signup. This allows users to browse courses and job experience of other
      users.
    - A future version of this application may include a search feature that
      lists users based on attributes of their profiles, such as users who have
      Master's degrees or who have worked in the "Computer Software" field.

### Advanced Features

1. Career recommendations
    - Users may get a list of recommended job titles/employers based on their
      entered profiles. The application searches for other users who have
      similar educational backgrounds and ranks the results by the similarity of
      the users' histories.
    - A future version of this application may include
      more sophisticated matching, including more profile details such as course
      history, GPA, and prior work experience. This is a data analysis problem
      and was beyond the scope of this project.

2. Course recommendations
    - Users may get a list of recommended courses to take based on their desired
      career industry. The user may additionally specify a job title which will
      further refine the results. If the user is interested only in classes at a
      particular university (as one would be if already enrolled and not looking
      for transfer credit), they may further specify the university name which
      will restrict the results to just that university.
    - A future version of this application may include more sophisticated
      recommendations. The current algorithm is a naive approach which counts
      the number of people with matching criteria taking each class and the
      number of those who don't match the criteria. If the difference between
      these is high enough, the class is considered relevant for the given
      industry. More details from the course catalogs (such as topic area) and
      more advanced weighting criteria could improve this metric. This is mostly
      a data analysis problem and was beyond the scope of this project.

### Easter Egg

1. Popular employers
    - We also included an API function which is not exposed in the user
      interface. This function returns a list of the top employers who have
      hired students from a particular university.

## User Interface Dataflow

The web interface was bootstrapped by using React.js. There are two pages in the
user interface. The first one is for onboarding the user in which a user profile
is created. The home page contains a dynamic form that allows a user to enter
his/her basic background information (ie. educational background and work
experience). After filling out the required information for each field (or
removing the default entries) the user may submit the form. A JSON object that
represents a user is then constructed and send to the backend server for
insertion. Finally, the user will receive a userID. This unique ID is used for
search, update, delete, and job recommendations.  For updating, searching, and
deleting a user, userID is required. Since the primary purpose of our
application is to offer insights based on data analytics, user authentication
will not be suitable and necessary.

The second page is a dashboard for users to interact with the database. Here
they may select an interaction type using the dropdown menu on the left. Choices
include Search, Update, and Delete which all accept a userID. The final option
is "Query" which presents the user with two additional options: search for jobs
or search for courses. The text entry field changes based on the user selection
to either accept a userID or a combination of industry, job title, and school
name for job search and course search, respectively.

On each of the dashboard pages, the results are shown below the search bar. For
Search, the user profile is shown in a resume-like format, displaying all
education and work history, including classes taken. In Update, the user profile
is automatically filled into the same form used on the onboarding page to allow
the user to amend the information.  Delete does not show any results other than
a status message. The query page will display a list of results, either jobs or
courses sorted by university.

## Basic Functions

At the very back of our backend, sitting right above the database engine, we
have a series of CRUD operations for each entity, including the relationship
tables. As these are implemented in Python-wrapped SQL calls, we are able to do
type-, value-, and error-checking without relying on the DBMS to fail in
expected ways.  We did also include check constraints and foreign key
constraints in the DB itself, but handling errors in Python was much more
manageable.

Just above the CRUD functions, we had the basic user functions. Since the notion
of a "user" to the app is more like a super entity to the DB, the create_user
method primarily consists of parsing a JSON document and calling the CRUD
functions for each entity type according to the data provided by the frontend.
As this is a lot of error checking and handling, we won't show the code here.
Check it out in [user.py](app/api/function/user.py) if you want to see more.

Beyond the basic CRUD functions, we also implemented a simple function which
returns the most popular employers for a given university.  We expose this as an
API which allows users to query for a particular university and specify the
maximum number of results.  The SQL query is shown below with a very basic
python wrapper which drops the count which was used for ordering and packages
the result up as a JSON object to return to the frontend.

### SQL Code

```python
def get_popular_companies(school: str, limit: int):
   rows = con.execute('''SELECT employerName, COUNT(userID) as cnt
                         FROM position p JOIN experience ex
                         ON p.id = ex.positionID
                         NATURAL JOIN education
                         WHERE university = ?
                         GROUP BY employerName
                         ORDER BY cnt DESC
                         LIMIT ?
                      ''', (school, limit)) \
             .fetchall()
   results = list(map(lambda x: x['employerName'], rows))
   return {'status': 'OK', 'results': results}
```

## Advanced Functions (AF1)

1. Career Recommendation – get a list of job titles that other users with
similar education have reported

    The goal here is to produce a list of job titles and associated employers
    which may be relevant to a particular user. There are many factors that may
    impact whether a job is relevant, but we only consider education to maintain
    a reasonable scope. Ideally, we would also consider a user's skills,
    interests, prior work experience, and specific course history.

    This feature is advanced because it utilizes several advanced features of
    the SQL language/engine. The first is the use of a view to simplify the
    primary query and (hopefully) improve performance slightly. The second is
    the use of a custom aggregate function. This is implemented in python, which
    is translated to C and linked into the DB engine using the SQLite C API.

    This feature is implemented as a single, albeit very complex SQL query. We
    must search all of the work experiences of all other users, pick out the
    ones held by users with some relation to the current one (in this case we
    are looking at alma maters and majors), and assign each title/employer combo
    a weight such that we may rank them from most relevant to least relevant.
    The following SQL code shows the query in its entirety. We will describe
    each part below.

    ```sql
    CREATE VIEW IF NOT EXISTS all_jobs (jobTitle, employerName, userID, university, degree, major)
    AS
       SELECT jobTitle, employerName, userID, university, degree, major
       FROM (education NATURAL JOIN experience) e
       JOIN position p
       ON e.positionID = p.id
       WHERE userID in (SELECT id FROM user);


    SELECT jobTitle || ', ' || employerName as job,
          CAREER_WEIGHT(
              a.major, a.university, a.degree, b.major, b.university, b.degree
          ) as weight
    FROM all_jobs a
    JOIN (
       SELECT userID, university, degree, major
       FROM education
       WHERE userID = :uid
    ) b
    ON a.userID <> b.userID
    GROUP BY jobTitle, employerName
    HAVING CAREER_WEIGHT(
       a.major, a.university, a.degree, b.major, b.university, b.degree
    ) > 0
    ORDER BY weight DESC
    LIMIT :limit;
    ```

    We have two primary queries here: A and B. Query B is easy, it just gets the
    User and Degree information of the user we want directly from the graduation
    table.

    Query A is harder. We start with the graduation table for the same info as
    B. Then we join with experience to gain access to the postionID column,
    which we then use to join with position, which gives us jobTitle and
    employerName. We filter this by users that still exist in the table (in case
    a user was deleted without removing their experience).  Query A now has
    access to each job title and employer combo held by each user, tagged by
    their university information.  We actually store query A as a view because
    it does not depend on any dynamic information from the query. This makes the
    main query easier and reduces the work of the DBMS as it only has to
    generate an execution plan once.  SQLite does not support materialized
    views, but if this we ported to another SQL implementation, an indexed view
    could also provide a performance increase by avoiding repeated joins and
    selections.

    Next, we join A and B on the condition that the userID is different,
    essentially getting a cross product of our user with the work experience of
    every other (active) user. We don't want to do a full cross product join
    because we don't want to associate a user's own work experience with
    themselves.

    We then group by the job info and aggregate using our custom weight function
    which takes the education info of our user and the other user to determine a
    relationship factor. We exclude the groups (jobs) which have no
    relationship.

    Finally, we select the job title and employer name concatenated together and
    the custom weight value and sort by descending weight.

2. Course Recommendation – get a list of courses that people who have that (or
similar) job took

    Our second advanced query is to retrieve a list of courses which may help a
    user on their path to a particular career goal. We approach this by asking
    the user which industry they are interested in working in, and whether they
    have a particular job title in mind (i.e. software engineer, analyst, sales,
    etc).  We then search over all users to find those who have had jobs in a
    similar industry (using approximate matching to include related industries,
    i.e. "computer" will include both "computer software" and "computer
    hardware") and optionally with a specific job title (using similar fuzzy
    matching). We also allow the user to specify which university they attend in
    order to restrict the class listing to their school.

    This feature is advanced because of the number of ways to combine the
    optional inputs makes it difficult to construct a single, general purpose
    query. We also have to consider that not all courses taken by someone who
    worked in your industry may be relevant (Gen Ed courses, campus
    requirements, etc). To combat this, we assign a positive weight to each
    course taken by someone in a matching industry/position and a negative
    weight to courses taken by people outside the industry. In this way, courses
    taken by everyone will have a zero (or nearly zero) net score. Courses taken
    primarily by people in the target group will have positive scores, and
    courses taken primarily by people not in the target group will have negative
    scores.

    We then remove courses with negative scores and apply a threshold to remove
    the ones with very low scores. The results are then sorted and limited to 20
    results per university. Universities which had all their results removed by
    the score filter are also removed.  The final Python querying, scoring, and
    filtering is shown below.  We omit the query construction as it is complex.

    ```python
    def course_recommendations(...):
        ...

        positive_query = base_query.format(query_join_position, where_clause)
        positive_matches = con.execute(positive_query, params).fetchall()

        negative_query = base_query.format(query_join_position, where_clause)
        negative_matches = con.execute(negative_query, params).fetchall()


        # build a dictionary mapping universities to courses, with each course having
        # an associate count (as a relevance metric)
        results = defaultdict(lambda: defaultdict(int))
        for row in positive_matches:
           results[row['university']][row['course']] += 1

        for row in negative_matches:
           results[row['university']][row['course']] -= 1

        # for each university (key k), sort the list of courses by relevance
        for k, v in results.items():
           # filter out any courses with negative relevance
           courses = list(filter(lambda x: x[1] > 0, v.items()))
           # courses = v.items()

           # sort alphabetically by course first, then sort by descending count
           # items with the same count wont be reordered
           courses = sorted(courses, key=lambda x: x[0], reverse=False)
           courses = sorted(courses, key=lambda x: x[1], reverse=True)
           # strip off the count, we don't need to show that to the user
           # also limit results to 20 per university
           results[k] = list(map(lambda x: x[0], courses))[:20]

        # elide any universities which have no courses
        results = {k: v for k, v in results.items() if len(v) > 0}

        if len(results) == 0:
           return ('', 204)
        return results
    ```

# Technical Challenge

The major challenge in the frontend is the dynamic form. The form allows a user
to dynamically add new fields depending on his/her background. It was
accomplished by using react state management. When a user clicks on the
plus/minus button to add or remove a form field, the corresponding react state
will add or remove an entry from the underlying array of the JSON object that
represents the current user. The component will then render to reflect the state
changes. Other react components such as the select component was used to allow
custom options to be added.  In addition, the accordion form design was adopted
to allow section divisions and collapse completed sections.

# Comparison with the Initial Plan

- We manually collect data instead of scraping data from career website due to
  the time limitation.
- Due to the time limitation and limited experience with data analysis, we were
  only able to implement 2 of the originally proposed advanced features. We had
  intended to have much more sophisticated and robust recommendation and search
  capabilities, but we were not able to achieve that in the given project
  timeframe.
- We ended up not using a NoSQL database, despite initially planning on using
  Neo4J as a social graph. We realized that the amount of effort required to
  implement such a feature would equal or exceed the total amount of effort for
  the whole project, making it infeasible for this assignment.

# Final Division of Labor

Daniel takes charge of the frond end, and Eric is responsible for most part of
the back end. Ning helps implement some advanced functions in the back end.  We
mostly worked on our respective components separately, communicating over Slack
for testing and design decisions.  Before campus closed, we had several
in-person meetings to discuss and plan project features and goals.
